// Server Classes 
abstract class Data {
    abstract public void DisplayValue();
}
class Integer extends  Data {
    int value;
    Integer() {
        value=300;
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}
class Float extends  Data {
    float value;
    Float() {
        value=2.688F;
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}
// Pattern Classes 
abstract class Factory {
    abstract public Data CreateDataObject();
}
class IntFactory extends Factory {
    public Data CreateDataObject(){
        return new Integer();
    }
}
class FloatFactory extends Factory {
    public Data CreateDataObject(){
        return new Float();
    }
}
class Document {    //Client classes
    Data pd;
    Document(Factory pf){
        pd = pf.CreateDataObject();
    }
    public void DisplayData(){
        pd.DisplayValue();
    }
}
//Test class
public class MyDoc {
    static Document d,f;
    public static void main(String[] args) {
        d = new Document(new IntFactory());
        d.DisplayData();
        f = new Document(new FloatFactory());
        f.DisplayData();
    }
}
