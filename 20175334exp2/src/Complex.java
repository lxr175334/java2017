public class Complex {
    //定义属性并生成getter，setter
    double RealPart;
    double ImagePart;
    public double getterRealPart() {
        return RealPart;
    }
    public double getterImagePart() {
        return ImagePart;
    }
    public void setter(double r, double i) {
        RealPart = r;
        ImagePart = i;
    }
    //定义构造函数
    public Complex(){

    }
    public Complex(double r, double i) {
        RealPart = r;
        ImagePart = i;
    }
    //Override Object
    public boolean equals(Object obj){
        if(this == obj){
            return true;
        }
        else
        {
            return false;
        }
    }
    public String toString() {
        String result = new String();
        if (ImagePart > 0)
            result = RealPart+"+"+ImagePart+"i";
        if (ImagePart == 0)
            result = RealPart+"";
        if (ImagePart < 0)
            result = RealPart+"-"+ImagePart*(-1)+"i";
        return result;
    }
    //定义公有方法：加减乘除
    public Complex ComplexAdd(Complex obj) {
        return new Complex ((getterRealPart()+obj.getterRealPart()),(getterImagePart()+obj.getterImagePart()));
    }
    public Complex ComplexSub(Complex obj) {
        return new Complex ((getterRealPart()-obj.getterRealPart()),(getterImagePart()-obj.getterImagePart()));
    }
    public Complex ComplexMulti(Complex obj) {
        return new Complex ((getterRealPart()*obj.getterRealPart()-getterImagePart()*obj.getterImagePart()),(getterRealPart()*obj.getterImagePart()+getterImagePart()*obj.getterRealPart()));
    }
    public Complex ComplexDiv(Complex obj){
        double div = Math.sqrt(obj.RealPart*obj.RealPart)+Math.sqrt(obj.ImagePart*obj.ImagePart);
        return new Complex(((RealPart*obj.RealPart+ImagePart*obj.ImagePart)/div),((ImagePart*obj.RealPart-RealPart*obj.ImagePart)/div));
    }
}
