import junit.framework.TestCase;
import org.junit.Test;
public class StringBufferDemoTest extends TestCase {
    StringBuffer buffer1 = new StringBuffer("StringBuffer");
    StringBuffer buffer2 = new StringBuffer("StringBufferStringBuffer");
    StringBuffer buffer3 = new StringBuffer("StringBufferStringBufferStringBuffer");
    @Test
    public void testcharAt() {
        assertEquals('S',buffer1.charAt(0));
        assertEquals('r',buffer1.charAt(11));
    }
    @Test
    public void testlength(){
        assertEquals(12,buffer1.length());
        assertEquals(24,buffer2.length());
        assertEquals(36,buffer3.length());
    }
    @Test
    public void testcapacity(){
        assertEquals(28,buffer1.capacity());
        assertEquals(40,buffer2.capacity());
        assertEquals(52,buffer3.capacity());
    }
    @Test
    public void testToString(){
        assertEquals("StringBuffer",buffer1.toString());
        assertEquals("StringBufferStringBuffer",buffer2.toString());
    }
    @Test
    public void testIndexOf(){
        assertEquals(0,buffer1.indexOf("Str"));
        assertEquals(5,buffer1.indexOf("gBu"));
    }
}
