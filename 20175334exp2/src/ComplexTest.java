import junit.framework.TestCase;
import org.junit.Test;
public class ComplexTest extends TestCase {
    Complex c1 = new Complex(2,4);
    Complex c2 = new Complex(-1,1);
    @Test
    public void testComplexAdd(){
        assertEquals("1.0+5.0i",c1.ComplexAdd(c2)+"");
    }
    @Test
    public void testComplexSub(){
        assertEquals("3.0+3.0i",c1.ComplexSub(c2)+"");
    }
    @Test
    public void testComplexMulti(){
        assertEquals("-6.0-2.0i",c1.ComplexMulti(c2)+"");
    }
    @Test
    public void testComplexDiv(){
        assertEquals("1.0-3.0i",c1.ComplexDiv(c2)+"");
    }
}
