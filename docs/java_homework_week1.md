# 20175334 《Java程序设计》第一周学习总结

## 教材学习内容总结

- 了解Java基础知识
- 下载、安装、学习JDK
- 编译、运行、调试简单的Java程序
- 熟悉掌握git部分常用指令

## 教材学习中的问题和解决过程

- 问题1：Linux上文件调用权限没有掌握
- 问题1解决方案：通过百度掌握chmod命令
- 问题2：主机上复制的内容无法粘贴至虚拟机上
- 问题2解决方案：虚拟机上依次点击“设备”——“共享粘贴板”——“双向”

## 代码调试中的问题和解决过程

- 问题1：在虚拟机上调试时候显示没有安装JDB
- 问题1解决方案：终端中依次输入命令 “sudo apt-get pdate”——“sudo apt-get install openjdk”

## [代码托管](https://gitee.com/lxr175334/java2017)

![](https://images.gitee.com/uploads/images/2019/0303/113402_f4d1dd89_4784454.jpeg)
![](https://images.gitee.com/uploads/images/2019/0303/113417_85622747_4784454.jpeg)

## 上周考试错题总结
- 错题1
![](https://images.gitee.com/uploads/images/2019/0303/113706_9b7549f2_4784454.jpeg)
- 原因：没有掌握“区分是否为主类不在于是否由public修饰，而是看是否含有main方法”
- 理解情况：已深刻记住

- 错题2
![](https://images.gitee.com/uploads/images/2019/0303/113716_15203052_4784454.jpeg)
- 原因：没有理解“srgs”是什么
- 理解情况：已掌握“args”为数组名称，可任意指定

- 错题3
![](https://images.gitee.com/uploads/images/2019/0303/113727_be0fe82e_4784454.jpeg)
- 原因：粗心，没有注意到题目信息
- 理解情况：已掌握

## 学习心得
- 作为一名新手，需要多敲代码去记住常用命令，不能靠死记硬背。
- 教学视频比自己看书更能深刻理解书本内容，必要时可以去网上查看参考资料。
- 学习java要合理安排时间，不要把任务都堆积到最后一天完成。

## 参考资料

- [使用码云和博客园学习简易教程](http://www.cnblogs.com/rocedu/p/6482354.html)
- [使用JDB调试Java程序](https://www.cnblogs.com/rocedu/p/6371262.html)
- [使用开源中国(码云)托管代码](http://www.cnblogs.com/rocedu/p/5155128.html3