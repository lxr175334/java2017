class A5_9 {
  final double PI=3.1415926;// PI是常量
  public double getArea(final double r) {
     return PI*r*r;
  }
  public final void speak() {
     System.out.println("您好，How's everything here ?");
  } 
}
public class Example5_9 {
   public static void main(String args[]) {
      A5_9 a=new A5_9();
      System.out.println("面积："+a.getArea(100));
      a.speak();     
   }
}

