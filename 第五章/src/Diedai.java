public class Diedai {
	public static void main(String args[]) {
		int []data = new int [args.length];
		int result;
		int n = 0;
		int m = 0;
		for(int i=0; i<args.length; i++) {
			data[i] = Integer.parseInt(args[i]);
		}
		n = data[0];
		m = data[1];
		result = Suanfa(n,m);
		if(result==0)
			System.out.println("输入有误!");
		else
			System.out.println(result);
	}
	public static int Suanfa(int n,int m) {
		if(m==1)
			return n;
		else if(m==0||m==n)
			return 1;
		else if(n<m||n==0)
			return 0;
		else
			return Suanfa(n-1,m-1)+Suanfa(n-1,m);
	}
}
