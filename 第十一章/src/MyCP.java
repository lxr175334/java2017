import java.io.*;
public class MyCP {
    public static void main(String []args) throws IOException {
        FileInputStream in = new FileInputStream(args[1]);
        FileOutputStream out = new FileOutputStream(args[2]);
        String str = new String(args[0]);
        byte[] data = new byte[1024];
        int length;
        if(str.equals("-tx")) {
            while((length = in.read(data)) != -1) {
                int n = data.hashCode();
                String binaryNum = Integer.toBinaryString(n);
                data = binaryNum.getBytes();
                out.write(data);
            }
        }
        else if(str.equals("-xt")) {
            while((length = in.read(data)) != -1) {
                int n = data.hashCode();
                String binaryNum = Integer.toString(n);
                data = binaryNum.getBytes();
                out.write(data);
            }
        }
    }
}

