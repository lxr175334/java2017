import junit.framework.TestCase;
import org.junit.Test;
public class ComplexTest extends TestCase {
    Complex c1 = new Complex(3,2);
    Complex c2 = new Complex(6,-2);
    @Test
    public void testComplexAdd(){
        assertEquals("9.0",c1.ComplexAdd(c2)+"");
    }
    @Test
    public void testComplexSub(){
        assertEquals("-3.0+4.0i",c1.ComplexSub(c2)+"");
    }
    @Test
    public void testComplexMulti(){
        assertEquals("22.0+6.0i",c1.ComplexMulti(c2)+"");
    }
    @Test
    public void testComplexDiv(){
        assertEquals("1.75+2.25i",c1.ComplexDiv(c2)+"");
    }
}

