/**
 * Complex class
 *
 * @author LYX
 * @date 2019/04/29
 */
public class Complex2{
    /**定义属性并生成getter，setter*/
    double realPart;
    double imagePart;
    public double getterRealPart() {
        return realPart;
    }
    public double getterImagePart() {
        return imagePart;
    }
    public void setter(double r, double i) {
        realPart = r;
        imagePart = i;
    }
    /**定义构造函数*/
    public Complex(){

    }
    public Complex(double r, double i) {
        realPart = r;
        imagePart = i;
    }
    /**Override Object*/
    @Override
    public boolean equals(Object obj){
        if(this == obj){
            return true;
        }
        else
        {
            return false;
        }
    }
    @Override
    public String toString() {
        String result = new String();
        if (imagePart > 0) {
            result = realPart + "+" + imagePart + "i";
        }
        if (imagePart == 0) {
            result = realPart + "";
        }
        if (imagePart < 0) {
            result = realPart + "-" + imagePart * (-1) + "i";
        }
        return result;
    }
    /**定义公有方法：加减乘除*/
    public Complex complexAdd(Complex obj) {
        return new Complex ((getterRealPart()+obj.getterRealPart()),(getterImagePart()+obj.getterImagePart()));
    }
    public Complex complexSub(Complex obj) {
        return new Complex ((getterRealPart()-obj.getterRealPart()),(getterImagePart()-obj.getterImagePart()));
    }
    public Complex complexmulti(Complex obj) {
        return new Complex ((getterRealPart()*obj.getterRealPart()-getterImagePart()*obj.getterImagePart()),(getterRealPart()*obj.getterImagePart()+getterImagePart()*obj.getterRealPart()));
    }
    public Complex complexDiv(Complex obj){
        double div = Math.sqrt(obj.realPart*obj.realPart)+Math.sqrt(obj.imagePart*obj.imagePart);
        return new Complex(((realPart*obj.realPart+imagePart*obj.imagePart)/div),((imagePart*obj.realPart-realPart*obj.imagePart)/div));
    }
}

