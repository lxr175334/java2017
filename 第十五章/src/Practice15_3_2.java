import java.util.*;
class Student15_3_2 implements Comparable {
	int english=0;
	String name;
	Student15_3_2(int english,String name) {
		this.name=name;
		this.english=english;
	}
	public int compareTo(Object b) {
		Student15_3_2 st=(Student15_3_2)b;
		return (this.english-st.english);
	}
}
public class Practice15_3_2 {
	public static void main(String atgs[]) {
		List<Student15_3_2>list=new LinkedList<Student15_3_2>();
		int score []={65,76,45,99,77,88,100,79};
		String name[]={"张三","李四","旺季","加戈","为哈","周和","赵李","将集"};
		for(int i=0; i<score.length; i++) {
			list.add(new Student15_3_2(score[i],name[i]));
		}
		Iterator<Student15_3_2> iter=list.iterator();
		TreeSet<Student15_3_2> mytree=new TreeSet<Student15_3_2>();
		while(iter.hasNext()) {
			Student15_3_2 stu=iter.next();
			mytree.add(stu);
		}
		Iterator<Student15_3_2> te=mytree.iterator();
		while(te.hasNext()) {
			Student15_3_2 stu=te.next();
			System.out.println(""+stu.name+""+stu.english);
		}
	}
}
