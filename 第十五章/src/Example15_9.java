import java.util.*;
class StudentKey implements Comparable { 
   double d=0; 
   StudentKey (double d) {
     this.d=d;
   }
   public int compareTo(Object b) {
     StudentKey st=(StudentKey)b;
     if((this.d-st.d)==0)
        return -1;
     else
        return (int)((this.d-st.d)*1000);
  }
}
class Student15_9 { 
    String name=null;
    double math,english;
    Student15_9(String s,double m,double e) {
       name=s; 
       math=m;
       english=e;
    }
}
public class Example15_9 {
   public static void main(String args[ ]) {
      TreeMap<StudentKey,Student15_9>  treemap= new TreeMap<StudentKey,Student15_9>();
      String str[]={"赵一","钱二","孙三","李四"};
      double math[]={89,45,78,76};
      double english[]={67,66,90,56};
      Student15_9 student[]=new Student15_9[4];
      for(int k=0;k<student.length;k++) {
         student[k]=new Student15_9(str[k],math[k],english[k]);
      }
      StudentKey key[]=new StudentKey[4] ;
      for(int k=0;k<key.length;k++) {
         key[k]=new StudentKey(student[k].math); //关键字按数学成绩排列大小
      }
      for(int k=0;k<student.length;k++) {
         treemap.put(key[k],student[k]);          
      }
      int number=treemap.size();
      System.out.println("树映射中有"+number+"个对象,按数学成绩排序:");
      Collection<Student15_9> collection=treemap.values();
      Iterator<Student15_9> iter=collection.iterator();
      while(iter.hasNext()) {
         Student15_9 stu=iter.next();
         System.out.println("姓名 "+stu.name+" 数学 "+stu.math);
      }
      treemap.clear();
      for(int k=0;k<key.length;k++) {
         key[k]=new StudentKey(student[k].english);//关键字按英语成绩排列大小
      }
      for(int k=0;k<student.length;k++) {
         treemap.put(key[k],student[k]);
      }
      number=treemap.size();
      System.out.println("树映射中有"+number+"个对象:按英语成绩排序:");
      collection=treemap.values();
      iter=collection.iterator();
      while(iter.hasNext()) {
         Student15_9 stu=(Student15_9)iter.next();
         System.out.println("姓名 "+stu.name+" 英语 "+stu.english);
      }
    }
}
