/**
 * @Author 16487
 */
public class Calc{
    public static void main (String [] args){

        int result = 0;

        if (args.length != 3){
            System.out.println("Usage: java Calc operator1 operand(+ - * / %) operator2");
        }
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[2]);
        switch (args[1]){
            case "+":
                result = a + b;
                break;
            case "-":
                result = a - b;
                break;
            case "x":
                result = a * b;
                break;
            case "/":
                result = a / b;
                break;
            case "%":
                result = a % b;
                break;
	    default:
                System.out.println("Error!");
                break;
        }
        System.out.println(args[0] + " " + args[1] + " " + args[2] + " = " + result);
    }
}
