import java.util.*;
class Student15_8 implements Comparable {
   int english=0;
   String name;
   Student15_8(int english,String name) {
      this.name=name;
      this.english=english;
   }
   public int compareTo(Object b) {
      Student15_8 st=(Student15_8)b;
      return (this.english-st.english);
   }
}
public class Example15_8 {
  public static void main(String args[]) {
     TreeSet<Student15_8> mytree=new TreeSet<Student15_8>();
     Student15_8 st1,st2,st3,st4;
     st1=new Student15_8(90,"赵一");
     st2=new Student15_8(66,"钱二");
     st3=new Student15_8(86,"孙三");
     st4=new Student15_8(76,"李四");
     mytree.add(st1);
     mytree.add(st2);
     mytree.add(st3);
     mytree.add(st4);
     Iterator<Student15_8> te=mytree.iterator();
     while(te.hasNext()) {
        Student15_8 stu=te.next();
        System.out.println(""+stu.name+" "+stu.english);
     }
  }
}

