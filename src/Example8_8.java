import java.util.*; public class Example8_8 {     
public static void main(String args[]) {        
String sourceString = "今晚十点进攻";        
System.out.println("通过随机密钥加密:"+sourceString);        
EncryptAndDecrypt person = new EncryptAndDecrypt();        
Random random = new Random();        
String password = Integer.toString(random.nextInt());        
System.out.println("随机密钥为:"+password);        
String secret = person.encrypt(sourceString,password);        
System.out.println("密文:"+secret);        
String source = person.decrypt(secret,password);        
System.out.println("明文:"+source);     
} 
}
