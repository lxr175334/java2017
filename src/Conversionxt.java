import java.io.*;
public class Conversionxt {
    public static void dump(Reader src, Writer dest) throws IOException {
        try(Reader input = src; Writer output = dest) {
            char[] data = new char[8];
            char []ch = new char[1];
            while((input.read(data)) != -1) {
                String str = "";
                for(int i=0; i<8; i++)
                {
                    str += data[i];
                }
                int data2 = Integer.valueOf(str,2);
                ch[0] = (char)data2;
                output.write(ch, 0, 1);
                output.write(" ");
            }
        }
    }
}
