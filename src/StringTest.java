import org.junit.Test;
import junit.framework.TestCase;
public class StringTest extends TestCase {
    String str = "I love Java";
    @Test
    public void testCharAt() {
        assertEquals('I',str.charAt(0));//正常情况
        assertEquals('a',str.charAt(10));//边界情况
        assertEquals('e',str.charAt(3));//异常情况
    }
    @Test
    public void testSplit() {
        assertEquals("love", str.split(" ")[1]);//正常情况
        assertEquals("I ", str.split("[a-z]+ ")[0]);//边界情况
        assertEquals("Java", str.split(" ")[0]);//异常情况
    }
}
