import java.io.*;
import java.util.*;
public class Text_file_conversion {
    public static void main(String []args) throws IOException {
	String file = "test.txt";
        String s =dataInputStream(file);//输入文本文件
        FileOutputStream out = new FileOutputStream("result.txt");//创建输出流
        out.write(s.getBytes());//向目的地写数据
        out.close();//关闭流
    }
    public static String dataInputStream(String file) throws IOException {
        File file2 = new File(file);//创建File对象
        DataInputStream input = new DataInputStream(new FileInputStream(file2));//数据输出流
        StringBuilder Builder = new StringBuilder();//可变字符序列
        byte by = 0;
        for(int i=0; i<file2.length(); i++) {
            by = input.readByte();//读取并返回输入字节
            String str = Integer.toBinaryString(by);//以二进制无符号整数形式返回一个整数参数的字符串表示形式
            if(str.length() == 1) {
                str = "0"+str;
            }
            Builder.append(str.toUpperCase());//小写字母转为大写字母并加入数组中
        }
        return Builder.toString();
    }
}
