import junit.framework.TestCase;
import org.junit.Test;
import java.util.Arrays;
import static java.util.Arrays.binarySearch;
public class ArraysTest extends TestCase {
    int[] i = {3, 2, 1, 4};
    @Test
    public void testsort() {
        Arrays.sort(i);
        assertEquals(2,  i[1]); //正常情况
        assertEquals(1, i[2]); //错误情况
    }
    @Test
    public void testbinarySarch() {
        Arrays.sort(i);
        assertEquals(0, binarySearch(i,1)); //正常情况
        assertEquals(3, binarySearch(i,4)); //边界情况
        assertEquals(1, binarySearch(i,3)); //异常情况
        }
}
