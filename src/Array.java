public class Array {
    public static void main(String[] args) {
        int arr[] = {1,2,3,4,5,6,7,8};
        for(int i:arr){
            System.out.print(i+" ");
        }
        System.out.println();
	   int temp1 = -1;
        for(int i:arr) {
            if(arr[i] == 5){
                temp1 = i;
                break;
            }
        }
        for(int i=temp1+1;i<arr.length;i++){
            arr[i-1] = arr[i];
        }
        arr[arr.length-1] = 0;
        for(int i:arr){
            System.out.print(i+" ");
        }
        System.out.println();
	int temp2 = 0;
	for(int i:arr){
            if(arr[i] == 4){
                temp2 = i;
                break;
            }
        }
        for(int i=arr.length-1;i>temp2+1;i--){
            arr[i] = arr[i-1];
        }
        arr[temp2+1] = 5;
        for(int i:arr){
            System.out.print(i+" ");
        }
        System.out.println();
    }
}
