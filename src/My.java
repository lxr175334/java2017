import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
public class My {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();//读取输入的数据
        String[]Nstr = str.split(" ");//用空格分割字符串
        if(Nstr[0].equals("-tx"))
        {
            FileReader reader = new FileReader(Nstr[1]);//文件字符输入
            FileWriter writer = new FileWriter(Nstr[2]);//文件字符输出
            Conversiontx.dump(reader, writer);
        }
        if(Nstr[0].equals("-xt"))
        {
            FileReader reader = new FileReader(Nstr[1]);//文件字符输入
            FileWriter writer = new FileWriter(Nstr[2]);//文件字符输出
            Conversionxt.dump(reader, writer);
        }
    }
}
