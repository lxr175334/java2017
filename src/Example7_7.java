public class Example7_7 {
   public static void main(String args[]) {
      CargoBoat ship = new CargoBoat();
      ship.setMaxContent(1000);
      ship.setMinContent(334);
      int m =200;
      try{  
           ship.loading(m);
      }
      catch(DangerException2 e) {
           System.out.println(e.warnMess()); 
           System.out.println("当前一共装载重量是"+m+"吨的集装箱");       
      }
      finally {
          System.out.printf("货船将正点启航");
      }
  }
}
