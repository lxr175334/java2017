import java.io.*;
public class Conversiontx {
    public static void dump(Reader src, Writer dest) throws IOException {
        try(Reader input = src; Writer output = dest) {
            char[] data = new char[1];
            while((input.read(data)) != -1) {
                int data1 = (int)data[0];
                String str1 = "";
                String str = Integer.toBinaryString(data1);
                for(int i=0; i<8; i++)
                {
                    if(i<(8-str.length()))str1 += '0';

                }
                str1 += str;
                output.write(str1, 0, 8);
                output.write(" ");
            }
        }
    }
}
