/**
 * @Author 16487
 */
import java.util.Stack;

public class MyBC1 {
    public MyBC1(){}
    public static String infixToSuffix(String exp){
        Stack<String> s = new Stack<String>();         // 创建操作符堆栈
        String suffix = "";            // 要输出的后缀表达式字符串
        String suffix1 = "";             //上一次的后缀表达式
        String suffix2 = "";
        String str[] = exp.split(" ");
        int length = str.length; // 输入的中缀表达式的长度
        String temp="";
        for (int i = 0; i < length; i++) {            // 对该中缀表达式的每一个字符并进行判断
            switch (str[i]) {
                case " ":break;           // 忽略空格
                case "(":
                    s.push(str[i]);                  // 如果是左括号直接压入堆栈
                    break;
                case "+":
                case "-":
                    if(s.size() != 0){          // 碰到'+' '-'，将栈中的所有运算符全部弹出去，直至碰到左括号为止，输出到队列中去
                        temp = s.pop();
                        if (temp.equals("(")) {     // 将左括号放回堆栈，终止循环
                            s.push(temp);
                            s.push(str[i]);
                            break;
                        }
                        else{
                            s.push(str[i]);
                            suffix2 = suffix2 + temp + " ";
                            break;
                        }
                    }
                    else{
                        s.push(str[i]);      // 说明是当前为第一次进入或者其他前面运算都有括号等情况导致栈已经为空,此时需要将符号进栈
                        break;
                    }
                    // 如果是乘号或者除号，则弹出所有序列，直到碰到加好、减号、左括号为止，最后将该操作符压入堆栈
                case "*":
                case "÷":
                    if(s.size()!=0){
                        temp = s.pop();
                        if(temp.equals("+")||temp.equals("-")||temp.equals("(")){
                            s.push(temp);
                            s.push(str[i]);
                            break;
                        }
                        else{
                            s.push(str[i]);
                            suffix2 = suffix2+temp+" ";
                            break;
                        }
                    }
                    else {
                        s.push(str[i]);     //当前为第一次进入或者其他前面运算都有括号等情况导致栈已经为空,此时需要将符号进栈
                        break;
                    }
                    // 如果碰到的是右括号，则距离栈顶的第一个左括号上面的所有运算符弹出栈并抛弃左括号
                case ")":
                    while (!s.isEmpty()) {
                        temp = s.pop();
                        if (temp.equals("(")) {
                            break;
                        } else {
                            suffix2 = suffix2+temp+" ";
                        }
                    }
                    break;
                // 默认情况，如果读取到的是数字，则直接送至输出序列
                default:
                    suffix2 = suffix2+str[i]+" ";
                    break;
            }

        }
        // 如果堆栈不为空，则把剩余运算符一次弹出，送至输出序列
        while (s.size() != 0) {
            suffix2 = suffix2+s.pop()+" ";
        }
        if(suffix1.equals("")){          //第一个题目
            suffix1 = suffix2;
            suffix = suffix2;
        }
        else{
            if(suffix2.equals(suffix1))
                suffix = "";
            else
                suffix = suffix2;
        }
        suffix1 = suffix2;
        return suffix;
    }
}


