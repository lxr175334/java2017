public class Test {
	public static void main(String args[]) {
		CPU cpu1 = new CPU();
		CPU cpu2 = new CPU();
		cpu1.speed = 2200;
		cpu2.speed = 2300;
		HardDisk HD1 = new HardDisk();
		HardDisk HD2 = new HardDisk();
		HD1.amount = 200;
		HD2.amount = 200;
		PC pc1 = new PC();
		PC pc2 = new PC();
		pc1.setCPU(cpu1);
                pc1.setHardDisk(HD1);
                pc1.show();
                pc2.setCPU(cpu2);
                pc2.setHardDisk(HD2);
		pc2.show();
                System.out.println("pc1.toString()="+pc1.toString());
                System.out.println("cpu1.toString()="+cpu1.toString());
                System.out.println("HD1.toString()="+HD1.toString());
                System.out.println("pc2.toString()="+pc2.toString());
                System.out.println("cpu2.toString()="+cpu2.toString());
                System.out.println("HD2.toString()="+HD2.toString());
                System.out.println("PC.equals(pc1,pc2)="+PC.equals(pc1,pc2));
                System.out.println("CPU.equals(cpu1,cpu2)="+CPU.equals(cpu1,cpu2));
                System.out.println("HardDisk.equals(HD1,HD2)="+HardDisk.equals(HD1,HD2));
	}
}
