/**
 * Complex class
 *
 * @Author 16487
 * @date 2019/4/29
 */

import junit.framework.TestCase;
import org.junit.Test;

public class lyxcomplexTest extends TestCase {
    lyxcomplex a=new lyxcomplex(0,0);
    lyxcomplex b=new lyxcomplex(1,1);
    lyxcomplex c=new lyxcomplex(-1,-1);
    lyxcomplex d=new lyxcomplex(20.16,53.10);
    lyxcomplex e=new lyxcomplex(2,3);
    @Test
    public void testgetReal(){
        assertEquals(0.0,a.getRealPart());
        assertEquals(-1.0,c.getRealPart());
        assertEquals(20.16,d.getRealPart());
    }
    @Test
    public void testgetIma(){
        assertEquals(0.0,a.getImagePart());
        assertEquals(-1.0,c.getImagePart());
        assertEquals(53.1,d.getImagePart());
    }
    @Test
    public void testComAdd(){
        assertEquals("0.0",b.complexAdd(c).toString());
        assertEquals("1.0+i",a.complexAdd(b).toString());
        assertEquals("19.16+52.1i",c.complexAdd(d).toString());
        assertEquals("-1.0-i",a.complexAdd(c).toString());
        assertEquals("21.16+54.1i",b.complexAdd(d).toString());
        assertEquals("20.16+53.1i",a.complexAdd(d).toString());
    }
    @Test
    public void testComSub(){
        assertEquals("1.0+i",b.complexSub(a).toString());
        assertEquals("-21.16-54.1i",c.complexSub(d).toString());
        assertEquals("2.0+2.0i",b.complexSub(c).toString());
        assertEquals("-1.0-i",a.complexSub(b).toString());
    }
    @Test
    public void testComMul(){
        assertEquals("0.0",a.complexMulti(d).toString());
        assertEquals("-1.0-i",b.complexMulti(c).toString());
        assertEquals("-20.16-53.1i",c.complexMulti(d).toString());
        assertEquals("40.32+159.3i",d.complexMulti(e).toString());
        assertEquals("1.0+i",b.complexMulti(b).toString());
    }
    @Test
    public void  testComDiv(){
        assertEquals("0.0",a.complexDiv(b).toString());
        assertEquals("-1.0-i",c.complexDiv(b).toString());
        assertEquals("-0.5-0.3333333333333333i",c.complexDiv(e).toString());
        assertEquals("10.08+17.7i",d.complexDiv(e).toString());
        assertEquals("20.16+53.1i",d.complexDiv(b).toString());
    }
}

