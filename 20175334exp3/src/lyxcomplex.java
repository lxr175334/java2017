/**
 * Complex class
 *
 * @Author 16487
 * @date 2019/4/29
 */

public class lyxcomplex {
    double a,b;
    lyxcomplex(double m, double n){/**构造函数设置实部虚部*/
        a=m;
        b=n;
    }
    public double getRealPart(){//返回实部
        return a;
    }

    public double getImagePart() {/**返回虚部*/
        return b;
    }
    public lyxcomplex complexAdd(lyxcomplex y){/**加法*/
        double m=y.getRealPart();
        double n=y.getImagePart();
        double x=a+m;
        double z=b+n;
        return new lyxcomplex(x,z);
    }
    public lyxcomplex complexSub(lyxcomplex y){/**减法*/
        double m=y.getRealPart();
        double n=y.getImagePart();
        double x=a-m;
        double z=b-n;
        return new lyxcomplex(x,z);
    }
    public lyxcomplex complexMulti(lyxcomplex y){/**乘法*/
        double m=y.getRealPart();
        double n=y.getImagePart();
        double x=a*m;
        double z=b*n;
        return new lyxcomplex(x,z);
    }
    public lyxcomplex complexDiv(lyxcomplex y){/**除法*/
        double m=y.getRealPart();
        double n=y.getImagePart();
        double x=a/m;
        double z=b/n;
        return new lyxcomplex(x,z);
    }
    @Override
    public java.lang.String toString() {
        String s="";
        if (a!=0&&b>0&&b!=1){
            s+= a+"+"+ b+"i";
        }
        else if(a!=0&&b==1){
            s+=a+"+i";
        }
        else if (a!=0&&b<0&&b!=-1){
            s+= a+""+b+"i";
        }
        else if (a!=0&&b==-1){
            s+=a+"-i";
        }
        else if (a!=0&&b==0){
            s+=a;
        }
        else if (a==0&&b!=0){
            s+=b+"i";
        }
        else if (a==0&&b==0){
            s+="0.0";
        }
        return s;
    }
}

