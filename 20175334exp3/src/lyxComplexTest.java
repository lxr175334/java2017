/**
 * @Author 16487
 */
import junit.framework.TestCase;
import org.junit.Test;

public class lyxComplexTest extends TestCase {
    lyxComplex a=new lyxComplex(0,0);
    lyxComplex b=new lyxComplex(1,1);
    lyxComplex c=new lyxComplex(-1,-1);
    lyxComplex d=new lyxComplex(20.16,53.10);
    lyxComplex e=new lyxComplex(2,3);
    @Test
    public void testgetReal(){
        assertEquals(0.0,a.getRealPart());
        assertEquals(-1.0,c.getRealPart());
        assertEquals(20.16,d.getRealPart());
    }
    @Test
    public void testgetIma(){
        assertEquals(0.0,a.getImagePart());
        assertEquals(-1.0,c.getImagePart());
        assertEquals(53.1,d.getImagePart());
    }
    @Test
    public void testComAdd(){
        assertEquals("0.0",b.ComplexAdd(c).toString());
        assertEquals("1.0+i",a.ComplexAdd(b).toString());
        assertEquals("19.16+52.1i",c.ComplexAdd(d).toString());
        assertEquals("-1.0-i",a.ComplexAdd(c).toString());
        assertEquals("21.16+54.1i",b.ComplexAdd(d).toString());
        assertEquals("20.16+53.1i",a.ComplexAdd(d).toString());
    }
    @Test
    public void testComSub(){
        assertEquals("1.0+i",b.ComplexSub(a).toString());
        assertEquals("-21.16-54.1i",c.ComplexSub(d).toString());
        assertEquals("2.0+2.0i",b.ComplexSub(c).toString());
        assertEquals("-1.0-i",a.ComplexSub(b).toString());
    }
    @Test
    public void testComMul(){
        assertEquals("0.0",a.ComplexMulti(d).toString());
        assertEquals("-1.0-i",b.ComplexMulti(c).toString());
        assertEquals("-20.16-53.1i",c.ComplexMulti(d).toString());
        assertEquals("40.32+159.3i",d.ComplexMulti(e).toString());
        assertEquals("1.0+i",b.ComplexMulti(b).toString());
    }
    @Test
    public void  testComDiv(){
        assertEquals("0.0",a.ComplexDiv(b).toString());
        assertEquals("-1.0-i",c.ComplexDiv(b).toString());
        assertEquals("-0.5-0.3333333333333333i",c.ComplexDiv(e).toString());
        assertEquals("10.08+17.7i",d.ComplexDiv(e).toString());
        assertEquals("20.16+53.1i",d.ComplexDiv(b).toString());
    }
}

